/*
* Bruce Cain
*
*
*/

#include <stdio.h>
#include "Terminal.h"

int main(void)
{
    char *input = getUserInput("Enter 5 letters: ", 5, ISALPHA|EXACT);

    printf("\nNumber entered: %s\n", input);
    free(input);
    return 0;
}

